import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from './Employee';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(private http : HttpClient) { }

  httpoptions = {
    headers : new HttpHeaders({
      'Content-Type':'application/json'
    })
  }

  getEmployees() : Observable<Employee> {
    return this.http.get<Employee>('../assets/Employees.json')
  }
}
