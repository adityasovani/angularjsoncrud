import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  employees: any = []
  id: number = null
  name: string = null
  role: string = null
  age: number = null
  isUpdating: boolean = false
  index: number = null

  constructor(private ds: DataService, private router : Router) {
  }

  ngOnInit() {
    this.loademployees()
  }

  loademployees() {
    this.employees = this.ds.getEmployees().subscribe(
      (data: any) => {
        this.employees = data
      }
    )
  }

  addEmployee() {
    if (this.isUpdating) {
      this.employees[this.index].id = this.id
      this.employees[this.index].name = this.name
      this.employees[this.index].age = this.age
      this.employees[this.index].role = this.role
    } else {
      this.employees.push({ id: this.id, name: this.name, role: this.role, age: this.age })
    }
    this.role = this.id = this.age = this.name = null
  }

  delete(index) {
    this.employees = this.employees.filter(Employee => Employee.id != index)
  }

  update(index) {
    this.id = this.employees[index].id
    this.name = this.employees[index].name
    this.role = this.employees[index].role
    this.age = this.employees[index].age
    this.isUpdating = true
    this.index = index
  }

  onSelectEmployee(e){
    this.router.navigate(['/selectedprod',e.id,e.name,e.age,e.role])
    console.log(e.id);
    
  }
}
