import { ActivatedRoute } from '@angular/router';
import { OnInit } from '@angular/core'
import { Component } from "@angular/core";

@Component({
    template: `
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h3>Employee Details</h3>
                <table class="table table-bordered">
                    <tr>
                        <th>id</th>
                        <td> {{id}} </td>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <td> {{name}} </td>
                    </tr>
                    <tr>
                        <th>Age</th>
                        <td> {{age}} </td>
                    </tr>
                    <tr>
                        <th>Role</th>
                        <td> {{role}} </td>
                    </tr>
                </table>
            </div>
        </div>
    `
})

export class SelectedEmployee implements OnInit {

    id: number = null
    name: String = null
    age: number = null
    role: string = null

    constructor(private ar: ActivatedRoute) {

        this.name = this.id = this.age = this.role = null

        let name = this.ar.snapshot.params['nam']
        let id = this.ar.snapshot.params['id']
        let age = this.ar.snapshot.params['age']
        let role = this.ar.snapshot.params['role']

        this.name = name
        this.id = id
        this.age = age
        this.role = role
    }

    ngOnInit() {

    }
}