import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SelectedEmployee } from './SelectedEmp';

const routes: Routes = [
  { path:'selectedprod/:nam/:id/:age/:role', component:SelectedEmployee }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
